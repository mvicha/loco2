Hosting Platform: AWS

This plan will ensure consistent response times thanks to the AWS auto-scaling
capabilities. It is going to monitor the CPU usage and will launch/remove instances
when required to guarantee the response of the application.

After executing this plan you will obtain an Elastic Beanstalk (EBS) Application
running within an Elastick Load Balancer with Auto Scaling capabilities.

You can simply upload your code and Elastic Beanstalk automatically handles the
deployment, from capacity provisioning, load balancing, auto-scaling to application
health monitoring. At the same time, you retain full control over the AWS resources
powering your application and can access the underlying resources at any time.

Plan:
* Build the Docker image and copy it to docker hub.

* Create a privileged user in AWS using IAM. Get the required AWS_ACCESS_KEY and
AWS_SECRET_ACCESS_KEY. This is going to be required to execute the plan.

* If you don't have terraform installed, download and install it.

* Create a Terraform script for IaC:
This terraform script will perform the following tasks:

1) Create role policies to provide access to the different resources.
2) Start AWS Elastic Beanstalk.
3) Create Amazon S3 bucket for storing instructions to launch Docker instances.
4) Copy Dockerrun.aws.json file to configure Docker instances when executed from
Elastic Beanstalk.
5) Create Amazon Virtual Private Cloud (VPC): Internet Gateway, Routes, Subnets.
6) Create Security Groups.
7) Create AWS Elastic Beanstalk Application.
8) Configure AWS Elastic Beanstalk Application (Elastic Load Balancing, Auto Scaling)
9) Launch AWS Elastic Beanstalk Application.

Thanks to the Terraform Script we have just created we're able to edit a variables
file to adapt the solution to different scenarios.

Key variables to define:
instance_type = t2.micro
autoscale_lower_bound = 20
autoscale_upper_bound = 80
autoscale_min = 1
autoscale_max = 20

With the previous variables we may control how our Auto Scaling group will behave. We
may use more powerful instances changing the instance_type variable, adjust the Auto
Scaling limits for creating/removing instances with the autoscale_lower_bound and
autoscale_upper_bound variables, define what's the minimal and maximun size of our
Auto Scaling group with the autoscale_min and autoscale_max variables.

After modifying the variables you may initialize terraform using:
terraform init

Now you should export your AWS keys
export AWS_ACCESS_KEY=
export AWS_SECRET_ACCESS_KEY=

Execute the terraform plan using:
terraform apply

You may obtain the Solution from:
git@gitlab.com:mvicha/loco2.git

data "terraform_remote_state" "network" {
	backend = "s3"
	config {
		bucket = "loco2-terraform-state-prod"
		key    = "loco2.tfstate"
		region = "${var.region}"
	}
}

#output "key_name" {
#	description = "List of key names of instances"
#	value = ["${aws_elastic_beanstalk_environment.gljenkins-env-uat.EC2KeyName}"]
#}

#output "public_ip" {
#	description = "List of public IP addresses assigned to the instances, if applicable"
#	value = ["${aws_elastic_beanstalk_environment.gljenkins-env-uat.ip}"]
#}

#output "vpc_security_group_ids" {
#	description = "List of associated security groups of instances, if running in non-default VPC"
#	value = ["${aws_elastic_beanstalk_environment.gljenkins-env-uat.vpc_security_group_ids}"]
#}

output "security_group_id" {
	description = "Security group ID"
	value = "${aws_security_group.default.id}"
}

#output "subnet_id" {
#	description = "List of IDs of VPC subnets of instances"
#	value = ["${aws_elastic_beanstalk_environment.gljenkins-env-uat.subnet_id}"]
#}

output "dns" {
	description = "DNS To access"
	value = ["${aws_elastic_beanstalk_environment.loco-env-uat.cname}"]
}

resource "null_resource" "launchlocoapp" {
  depends_on = ["aws_elastic_beanstalk_environment.loco-env-uat"]
  provisioner "local-exec" {
    command = "aws elasticbeanstalk update-environment --application-name loco --environment-name loco-env-uat --version-label latest"
  }
}

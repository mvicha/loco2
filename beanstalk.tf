resource "aws_elastic_beanstalk_application" "loco-ebs" {
  name = "loco-ebs"
  description = "Loco Beanstalk"
}

resource "aws_elastic_beanstalk_environment" "loco-env-uat" {
  name = "loco-env-uat"
  application = "loco-ebs"
  #solution_stack_name = "64bit Amazon Linux 2018.03 v2.11.0 running Docker 18.03.1-ce"
  solution_stack_name = "${var.solution_stack_name}"
  cname_prefix = "locoapp"
  tier = "${var.tier}"
  wait_for_ready_timeout = "${var.wait_for_ready_timeout}"
  version_label = "${var.version_label}"

  setting {
    namespace = "aws:ec2:vpc"
    name = "VPCId"
    value = "${aws_vpc.loco-vpc.id}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name = "AssociatePublicIpAddress"
    value = "${var.associate_public_ip_address}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name = "Subnets"
    value = "${join(",", aws_subnet.public-subnet.*.id)}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name = "ELBSubnets"
    value = "${join(",", aws_subnet.public-subnet.*.id)}"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name = "RollingUpdateEnabled"
    value = "true"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name = "RollingUpdateType"
    value = "${var.rolling_update_type}"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name = "MinInstancesInService"
    value = "${var.updating_min_in_service}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:command"
    name = "DeploymentPolicy"
    value = "${var.rolling_update_type == "Immutable" ? "Immutable" : "Rolling"}"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name = "MaxBatchSize"
    value = "${var.updating_max_batch}"
  }

  ###=========================== Autoscale trigger ========================== ###

  setting {
    namespace = "aws:autoscaling:trigger"
    name = "MeasureName"
    value = "CPUUtilization"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name = "Statistic"
    value = "Average"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name = "Unit"
    value = "Percent"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name = "LowerThreshold"
    value = "${var.autoscale_lower_bound}"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name = "UpperThreshold"
    value = "${var.autoscale_upper_bound}"
  }

  ###=========================== Autoscale trigger ========================== ###

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "SecurityGroups"
    value = "${aws_security_group.default.id}"
  }

  /* setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "SSHSourceRestriction"
    value = "tcp, 22, ${var.ssh_source_restriction}"
  } */

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "InstanceType"
    value = "${var.instance_type}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "IamInstanceProfile"
    value = "${aws_iam_instance_profile.aws-elasticbeanstalk-ec2-role.name}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "EC2KeyName"
    value = "${var.key_name}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "RootVolumeSize"
    value = "${var.root_volume_size}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "RootVolumeType"
    value = "${var.root_volume_type}"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name = "Availability Zones"
    value = "${var.autoscaling_availability_zones}"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name = "MinSize"
    value = "${var.autoscale_min}"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name = "MaxSize"
    value = "${var.autoscale_max}"
  }

  setting {
    namespace = "aws:elb:loadbalancer"
    name = "CrossZone"
    value = "true"
  }

  /* setting {
    namespace = "aws:elb:listener"
    name = "ListenerProtocol"
    value = "HTTP"
  }

  setting {
    namespace = "aws:elb:listener"
    name = "InstancePort"
    value = "80"
  }

  setting {
    namespace = "aws:elb:listener"
    name = "ListenerEnabled"
    value = "${var.http_listener_enabled == "true" || var.loadbalancer_certificate_arn == "" ? "true" : "false"}"
  }

  setting {
    namespace = "aws:elb:listener:443"
    name = "ListenerProtocol"
    value = "HTTPS"
  }

  setting {
    namespace = "aws:elb:listener:443"
    name = "InstancePort"
    value = "80"
  }

  setting {
    namespace = "aws:elb:listener:443"
    name = "SSLCertificateId"
    value = "${var.loadbalancer_certificate_arn}"
  }

  setting {
    namespace = "aws:elb:listener:443"
    name = "ListenerEnabled"
    value = "${var.loadbalancer_certificate_arn == "" ? "false" : "true"}"
  } */

  setting {
    namespace = "aws:elb:listener:${var.ssh_listener_port}"
    name = "ListenerProtocol"
    value = "TCP"
  }

  setting {
    namespace = "aws:elb:listener:${var.ssh_listener_port}"
    name = "ListenerEnabled"
    value = "${var.ssh_listener_enabled}"
  }

  setting {
    namespace = "aws:elb:policies"
    name = "ConnectionSettingIdleTimeout"
    value = "${var.ssh_listener_enabled == "true" ? "3600" : "60"}"
  }

  setting {
    namespace = "aws:elb:policies"
    name = "ConnectionDrainingEnabled"
    value = "true"
  }

  /* setting {
    namespace = "aws:elbv2:loadbalancer"
    name = "AccessLogsS3Bucket"
    value = "${aws_s3_bucket.elb_logs.id}"
  }

  setting {
    namespace = "aws:elbv2:loadbalancer"
    name = "AccessLogsS3Enabled"
    value = "true"
  } */

  setting {
    namespace = "aws:elbv2:listener:default"
    name = "ListenerEnabled"
    value = "${var.http_listener_enabled == "true" || var.loadbalancer_certificate_arn == "" ? "true" : "false"}"
  }

  /* setting {
    namespace = "aws:elbv2:listener:443"
    name = "Protocol"
    value = "HTTPS"
  }

  setting {
    namespace = "aws:elbv2:listener:443"
    name = "SSLCertificateArns"
    value = "${var.loadbalancer_certificate_arn}"
  } */

  /* setting {
    namespace = "aws:elasticbeanstalk:healthreporting:system"
    name = "ConfigDocument"
    value = "${var.config_document}"
  } */

  setting {
    namespace = "aws:elasticbeanstalk:application"
    name = "Application Healthcheck URL"
    value = "HTTP:80${var.healthcheck_url}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name = "LoadBalancerType"
    value = "${var.loadbalancer_type}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name = "ServiceRole"
    value = "${aws_iam_role.ec2.name}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:healthreporting:system"
    name = "SystemType"
    value = "enhanced"
  }

  setting {
    namespace = "aws:elasticbeanstalk:command"
    name = "BatchSizeType"
    value = "Fixed"
  }

  setting {
    namespace = "aws:elasticbeanstalk:command"
    name = "BatchSize"
    value = "1"
  }

  setting {
    namespace = "aws:elasticbeanstalk:command"
    name = "DeploymentPolicy"
    value = "Rolling"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "BASE_HOST"
    value = "${var.name}"
  }

  /* setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "CONFIG_SOURCE"
    value = "${var.config_source}"
  } */

  setting {
    namespace = "aws:elasticbeanstalk:managedactions"
    name = "ManagedActionsEnabled"
    value = "true"
  }

  setting {
    namespace = "aws:elasticbeanstalk:managedactions"
    name = "PreferredStartTime"
    value = "${var.preferred_start_time}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
    name = "UpdateLevel"
    value = "${var.update_level}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
    name = "InstanceRefreshEnabled"
    value = "${var.instance_refresh_enabled}"
  }

  ###===================== Application Load Balancer Health check settings =====================================================###
  # The Application Load Balancer health check does not take into account the Elastic Beanstalk health check path
  # http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environments-cfg-applicationloadbalancer.html
  # http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environments-cfg-applicationloadbalancer.html#alb-default-process.config

  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name = "HealthCheckPath"
    value = "${var.healthcheck_url}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name = "Port"
    value = "80"
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name = "Protocol"
    value = "HTTP"
  }

  ###===================== Notification =====================================================###

  setting {
    namespace = "aws:elasticbeanstalk:sns:topics"
    name = "Notification Endpoing"
    value = "${var.notification_endpoint}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:sns:topics"
    name = "Notification Protocol"
    value = "${var.notification_protocol}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:sns:topics"
    name = "Notification Topic ARN"
    value = "${var.notification_topic_arn}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:sns:topics"
    name = "Notification Topic Name"
    value = "${var.notification_topic_name}" 
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "environment"
    value = "${var.ebs_app_environment}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "LOGGING_APPENDER"
    value = "GAYLOG"
  }

  /* setting {
    namespace = "aws:elbv2:loadbalancer"
    name = "ManagedSecurityGroup"
    value = "${aws_security_group.default.id}"
  } */

  tags {
    Team = "mvilla"
    Environment = "${var.ebs_app_environment}"
  }
}


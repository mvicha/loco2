data "aws_elb_service_account" "main" {}

/* data "aws_iam_policy_document" "elb_logs" {
  statement {
    sid = ""

    actions = [
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${var.logs_bucket_name}/*",
    ]

    principals {
      type = "AWS"
      identifiers = ["${data.aws_elb_service_account.main.arn}"]
    }

    effect = "Allow"
  }
}

resource "aws_s3_bucket" "elb_logs" {
  bucket = "${var.logs_bucket_name}"
  acl = "private"

  policy = "${data.aws_iam_policy_document.elb_logs.json}"
} */

resource "aws_s3_bucket" "loco-docker" {
  bucket = "loco-ebs"
}

resource "aws_s3_bucket_object" "loco-docker" {
  bucket = "${aws_s3_bucket.loco-docker.id}"
  key = "Dockerrun.aws.json"
  source = "Dockerrun.aws.json"
  #key = "locoapp.zip"
  #source = "locoapp.zip"
}

resource "aws_elastic_beanstalk_application_version" "loco-latest" {
  name = "latest"
  application = "${aws_elastic_beanstalk_application.loco-ebs.name}"
  description = "Version latest of app ${aws_elastic_beanstalk_application.loco-ebs.name}"
  bucket = "${aws_s3_bucket.loco-docker.id}"
  key = "${aws_s3_bucket_object.loco-docker.id}"

  /* provisioner "remote-exec" {
    inline = [
      "aws elasticbeanstalk update-environment --application-name loco --environment-name loco-env-uat --version-label latest"
    ]
  } */
}

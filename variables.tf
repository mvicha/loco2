variable "user_known_hosts" {
  description = "Needed to store public key of the servers"
  default = "~/.ssh/known_hosts"
}

variable "region" {
  description = "Region where the instance is going to be deployed"
  default = "us-east-1"
}

variable "type" {
  type = "string"
  default = "public"
  description = "Type of subnets to create (`private` or `public`)"
}

variable "availability_zones" {
  type = "list"
  description = "List of Availability Zones (e.g. `['us-east-1a', 'us-east-1b', 'us-east-1c']`)"
  default = ["us-east-1a", "us-east-1b"]
}

variable "autoscaling_availability_zones" {
  default = "Any 2"
  description = "Choose the number of AZs for your instances"
}

variable "instance_type" {
  description = "Type of instance to use"
  default = "t2.micro"
}

variable "associate_public_ip_address" {
  default     = "true"
  description = "Specifies whether to launch instances in your VPC with public IP addresses."
}

variable "solution_stack_name" {
  default = "64bit Amazon Linux 2018.03 v2.11.0 running Multi-container Docker 18.03.1-ce (Generic)"
  description = "Elastic Beanstalk stack, e.g. Docker, Go, Node, Java, IIS. [Read more](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/concepts.platforms.html)"
}

variable "wait_for_ready_timeout" {
  default = "20m"
}

variable "autoscale_lower_bound" {
  default = "20"
  description = "Minimum level of autoscale metric to add instance"
}

variable "autoscale_upper_bound" {
  default = "80"
  description = "Maximum level of autoscale metric to remove instance"
}

variable "name" {
  default = "loco2"
  description = "Solution name, e.g. 'app' or 'jenkins'"
}

variable "updating_min_in_service" {
  description = "Minimum number of instances in service"
  default = "1"
}

variable "updating_max_batch" {
  description = "Maximum number of instances in batch"
  default = "10"
}

variable "autoscale_min" {
  description = "AutoScaling Group min size"
  default = "1"
}

variable "autoscale_max" {
  description = "AutoScaling Group max size"
  default = "20"
}

variable "config_document" {
  default = "{ \"CloudWatchMetrics\": {}, \"Version\": 1}"
  description = "A JSON document describing the environment and instance metrics to publish to CloudWatch."
}

variable "healthcheck_url" {
  default = "/"
  description = "Application Health Check URL. Elastic Beanstalk will call this URL to check the health of the application running on EC2 instances"
}

variable "notification_protocol" {
  default = "email"
  description = "Notification protocol"
}

variable "notification_endpoint" {
  default = ""
  description = "Notification endpoint"
}

variable "notification_topic_arn" {
  default = ""
  description = "Notification topic arn"
}

variable "notification_topic_name" {
  default = ""
  description = "Notification topic name"
}

variable "loadbalancer_type" {
  default = "application"
  description = "Load Balancer type, e.g. 'application' or 'classic'"
}

variable "loadbalancer_certificate_arn" {
  default = ""
  description = "Load Balancer SSL certificate ARN. The certificate must be present in AWS Certificate Manager"
}

variable "http_listener_enabled" {
  default = "true"
  description = "Enable port 80 (http)"
}

variable "ssh_listener_enabled" {
  default = "false"
  description = "Enable ssh port"
}

variable "ssh_listener_port" {
  default = "22"
  description = "SSH port"
}

variable "config_source" {
  default = ""
  description = "S3 source for config"
}

variable "preferred_start_time" {
  default     = "Sun:10:00"
  description = "Configure a maintenance window for managed actions in UTC"
}

variable "update_level" {
  default     = "minor"
  description = "The highest level of update to apply with managed platform updates"
}

variable "instance_refresh_enabled" {
  default     = "true"
  description = "Enable weekly instance replacement."
}

variable "ebs_app_environment" {
  description = "EBS Application environment"
  default = "uat"
}

variable "key_name" {
  description = "AWS key name"
  default = "glhygieia"
}

variable "key_file" {
  description = "Private Key File"
  default = "~/.ssh/glhygieia.pem"
}

variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  type = "list"
  description = "CIDR for the Public Subnet"
  default = ["10.0.0.0/24", "10.0.1.0/24"]
}

variable "private_subnet_cidr" {
  description = "CIDR for the Private Subnet"
  default = "10.0.2.0/24"
}

variable "root_volume_size" {
  description = "The size of the EBS root volue"
  default = "8"
}

variable "root_volume_type" {
  description = "The type of the EBS root volue"
  default = "gp2"
}

variable "rolling_update_type" {
  default = "Health"
  description = "Set it to Immutable to apply the configuration change to a fresh group of instances"
}

variable "tier" {
  default = "WebServer"
  description = "Elastic Beanstalk Environment tier, e.g. ('WebServer', 'Worker')"
}

variable "version_label" {
  default = ""
  description = "Elastic Beanstalk Application version for deploy"
}

variable "logs_bucket_name" {
  default = "mvilla-loco-s3-logs"
  description = "Bucket to save log files into"
}

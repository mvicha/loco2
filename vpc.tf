resource "aws_vpc" "loco-vpc" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "Auxis VPC"
  }
}

resource "aws_subnet" "public-subnet" {
  count = "${length(var.availability_zones)}"
  vpc_id = "${aws_vpc.loco-vpc.id}"
  cidr_block = "${element(var.public_subnet_cidr, count.index)}"
  map_public_ip_on_launch = true
  availability_zone = "${element(var.availability_zones, count.index)}"
  tags = {
    Name =  "Subnet public ${element(var.availability_zones, count.index)}"
  }
}

resource "aws_subnet" "private-subnet" {
  count = 1
  vpc_id = "${aws_vpc.loco-vpc.id}"
  cidr_block = "${var.private_subnet_cidr}"
  availability_zone = "${element(var.availability_zones, count.index)}"
  tags = {
    Name = "Subnet private Virginia 1a"
  }
}

resource "aws_internet_gateway" "loco-gw" {
  vpc_id = "${aws_vpc.loco-vpc.id}"
  tags {
    Name = "InternetGateway"
  }
}

resource "aws_route" "internet-access" {
  route_table_id = "${aws_vpc.loco-vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.loco-gw.id}"
}

resource "aws_route" "private-route" {
  route_table_id  = "${aws_vpc.loco-vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.loco-gw.id}"
}

resource "aws_route_table_association" "public-subnet-association" {
  count = "${length(var.availability_zones)}"
  subnet_id = "${element(aws_subnet.public-subnet.*.id, count.index)}"
  route_table_id = "${aws_vpc.loco-vpc.main_route_table_id}"
}

resource "aws_route_table_association" "private-subnet-association" {
  subnet_id = "${aws_subnet.private-subnet.id}"
  route_table_id = "${aws_vpc.loco-vpc.main_route_table_id}"
}
